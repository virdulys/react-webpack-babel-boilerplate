# Boilerplate for React + Webpack 4 + Babel + ESLint
### Installing
```
yarn install
```
### Building
This defaults to development config. Add new configs and npm scripts in package.json for production as needed.
```
yarn build
```
### Running
This defaults to development config. Add new configs and npm scripts in package.json for production as needed.
```
yarn start
```